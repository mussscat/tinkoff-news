//
//  SFCoreDataController.swift
//  Tinkoff-news
//
//  Created by SMFedorov on 07/03/2017.
//  Copyright © 2017 Sergey Fedorov. All rights reserved.
//

import Foundation
import CoreData

enum TinkoffNewsEntity : String
{
    case News = "NewsEntity"
    
    func entityDescription(context:NSManagedObjectContext) -> NSEntityDescription
    {
        let entityDescription = NSEntityDescription.entity(forEntityName: self.rawValue, in: context)!
        return entityDescription
    }
}


class SFCoreDataController : NSObject
{
    var managedObjectContext : NSManagedObjectContext
    
    override init()
    {        
        guard let modelURL = Bundle.main.url(forResource: "Tinkoff_news", withExtension:"momd") else {
            fatalError("Error loading model from bundle")
        }
        
        guard let mom = NSManagedObjectModel(contentsOf: modelURL) else {
            fatalError("Error initializing mom from: \(modelURL)")
        }
        
        let psc = NSPersistentStoreCoordinator(managedObjectModel: mom)
        
        managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = psc
        
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let docURL = urls[urls.endIndex-1]
        let storeURL = docURL.appendingPathComponent("Tinkoff_news.momd")

        do
        {
            try psc.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeURL, options: nil)
        }
        catch
        {
            fatalError("Error migrating store: \(error)")
        }
    }
    
    func saveContext()
    {
        if self.managedObjectContext.hasChanges
        {
            do
            {
                try self.managedObjectContext.save()
            }
            catch
            {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}
