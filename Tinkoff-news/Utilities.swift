//
//  Utilities.swift
//  Tinkoff-news
//
//  Created by SMFedorov on 09/03/2017.
//  Copyright © 2017 Sergey Fedorov. All rights reserved.
//

import Foundation
import UIKit

protocol SFErrorHandlingDelegate
{
    func didFailToUpdateNews(error:Error)
}

extension String
{
    func attributedHTMLTextString() -> NSAttributedString?
    {
        return try? NSAttributedString(data: self.data(using: .utf8)!,
                                       options: [NSDocumentTypeDocumentAttribute     : NSHTMLTextDocumentType,
                                                 NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue],
                                       documentAttributes: nil)
    }
}

extension DateFormatter
{
    func convertDateToString(date: Date) -> String
    {
        self.dateFormat = "dd-MM-yyyy 'at' HH:mm"
        return self.string(from: date)
    }
}

func prepareDateValue(dictionary: ([String : AnyObject]), key: String) throws -> NSDate
{
    if let dateDictionary = dictionary[key] as? [String : AnyObject] {
        guard let returnDate = dateDictionary["milliseconds"] as? Double else {
            print("Date is missing in values from JSON")
            throw JSONDeserializationError.ResultCodeError
        }
        return NSDate(timeIntervalSince1970: returnDate/1000.0)
    }
    else
    {
        throw JSONDeserializationError.EmptyResponse
    }
}

func prepareStringValue(dictionary: ([String : AnyObject]), key: String) throws -> String
{
    guard let returnString = dictionary[key] as? String else {
        throw JSONDeserializationError.EmptyResponse
    }
    return returnString
}

func addAlertErrorHandler(error: Error) -> UIAlertController
{
    let alert = UIAlertController(title: "Error",
                                  message: error.localizedDescription,
                                  preferredStyle: UIAlertControllerStyle.alert)
    alert.addAction(UIAlertAction(title: "OK",
                                  style: UIAlertActionStyle.default,
                                  handler: nil))
    return alert
}
