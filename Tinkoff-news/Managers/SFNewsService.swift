//
//  SFNewsService.swift
//  Tinkoff-news
//
//  Created by SMFedorov on 13/03/2017.
//  Copyright © 2017 Sergey Fedorov. All rights reserved.
//

import Foundation
import CoreData

enum NewsDataSourceError : Error
{
    case UnexceptedFetchError
    case FetchFromCoreDataError
    case UnexceptedDataError
    case ErrorPopulatingData
}

class SFNewsService
{
    let coreDataManager = SFCoreDataController()
    let requestManager = SFRequestManager()
    
    func updateNewsList(success: @escaping(Void) -> (Void), failure: @escaping(Error) -> (Void))
    {
        requestManager.performRequest(method: .News, success: { [weak self] (payload) in
            
            let backgroundContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
            backgroundContext.parent = self?.coreDataManager.managedObjectContext
            backgroundContext.perform {
                if let arrayList = payload as? [[String : AnyObject]] {
                    for object in arrayList
                    {
                        do
                        {
                            try self?.insertOrDeleteNewsObjectWithData(object: object, inContext: backgroundContext)
                        }
                        catch
                        {
                            DispatchQueue.main.async {
                                failure(NewsDataSourceError.ErrorPopulatingData)
                            }
                        }
                    }
                    
                    var saveError:Error? = nil
                    
                    do
                    {
                        try backgroundContext.save()
                    }
                    catch
                    {
                        saveError = error
                    }
                    
                    DispatchQueue.main.async {
                        if let error = saveError
                        {
                            failure(error)
                        }
                        else
                        {
                            self?.coreDataManager.saveContext()
                            success()
                        }
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        failure(NewsDataSourceError.UnexceptedDataError)
                    }
                }
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func updateNewsDetails(newsID: String,
                           completion: @escaping (NewsEntity) -> Void, failure: @escaping (Error) -> (Void))
    {
        requestManager.performRequest(method: .NewsContent(newsID),
                                      success: { [weak self] (payload) in
                                        if let object = payload as? [String : AnyObject]
                                        {
                                            do
                                            {
                                                try self?.insertContentOrUpdateWithObject(object: object,
                                                                                          id: newsID)
                                            }
                                            catch
                                            {
                                                failure(NewsDataSourceError.ErrorPopulatingData)
                                            }
                                            
                                            self?.coreDataManager.saveContext()
                                            
                                            do
                                            {
                                                if let coreDataObject = try self?.findNewsObject(id: newsID,
                                                                                                 inContext: (self?.coreDataManager.managedObjectContext)!) {
                                                    completion(coreDataObject)
                                                }
                                            }
                                            catch
                                            {
                                                failure(NewsDataSourceError.UnexceptedDataError)
                                            }
                                        }
        }) { (error) in
            failure(error)
        }
    }
    
    private func insertOrDeleteNewsObjectWithData(object:[String : AnyObject],
                                          inContext:NSManagedObjectContext) throws
    {
        do
        {
            let id = try prepareStringValue(dictionary: object, key: NewsEntityKeys.ID.rawValue)
            
            do
            {
                var newsEntity: NewsEntity
                if let coreDataObject = try self.findNewsObject(id: id, inContext: inContext) {
                    newsEntity = coreDataObject
                }
                else
                {
                    let existingObject = NSManagedObject(entity: TinkoffNewsEntity.News.entityDescription(context: inContext),
                                                         insertInto:inContext) as! NewsEntity
                    existingObject.id = id
                    newsEntity = existingObject
                }
                do
                {
                    newsEntity.name = try prepareStringValue(dictionary: object,
                                                         key: NewsEntityKeys.name.rawValue).attributedHTMLTextString()?.string
                    newsEntity.publicationDate = try prepareDateValue(dictionary: object,
                                                                  key: NewsEntityKeys.publicationDate.rawValue)
                    newsEntity.text = try prepareStringValue(dictionary: object,
                                                         key: NewsEntityKeys.text.rawValue).attributedHTMLTextString()?.string
                }
                catch
                {
                    throw NewsDataSourceError.ErrorPopulatingData
                }
            }
            catch
            {
                throw NewsDataSourceError.ErrorPopulatingData
            }
        }
        catch
        {
            throw JSONDeserializationError.ResultCodeError
        }
    }
    
    private func insertContentOrUpdateWithObject(object: [String : AnyObject],
                                                 id: String) throws
    {
        do
        {
            var newsEntity: NewsEntity
            
            if let coreDataObject = try self.findNewsObject(id: id,
                                                                   inContext: self.coreDataManager.managedObjectContext)
            {
                newsEntity = coreDataObject
                if (newsEntity.contentText == nil)
                {
                    do
                    {
                        newsEntity.contentText = try prepareStringValue(dictionary: object,
                                                                    key: NewsEntityKeys.contentText.rawValue).attributedHTMLTextString()?.string
                    }
                    catch
                    {
                        throw NewsDataSourceError.ErrorPopulatingData
                    }
                }
            }
        }
        catch
        {
            throw NewsDataSourceError.ErrorPopulatingData
        }
    }
    
    private func findNewsObject(id:String,
                        inContext:NSManagedObjectContext) throws -> NewsEntity?
    {
        let context = inContext
        let fetchRequest = NSFetchRequest<NewsEntity>(entityName: TinkoffNewsEntity.News.rawValue)
        let predicate = NSPredicate(format: "id = %@", argumentArray: [id])
        fetchRequest.predicate = predicate
        
        do
        {
            let objects = try context.fetch(fetchRequest)
            return objects.first
        }
        catch
        {
            throw NewsDataSourceError.UnexceptedFetchError
        }
    }
}
