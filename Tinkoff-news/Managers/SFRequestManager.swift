//
//  SFRequestManager.swift
//  Tinkoff-news
//
//  Created by SMFedorov on 07/03/2017.
//  Copyright © 2017 Sergey Fedorov. All rights reserved.
//

import Foundation


enum HTTPMethod : String
{
    case GET = "GET"
    case POST = "POST"
}

enum TCSAPIMethod
{
    
    case News
    case NewsContent(String)
    
    func methodURL() -> URL
    {
        switch self
        {
            case     .News:             return URL(string: "https://api.tinkoff.ru/v1/news")!
            case let .NewsContent(id):  return URL(string: "https://api.tinkoff.ru/v1/news_content?id=\(id)")!
        }
    }
    
    func HTTPMethod() -> HTTPMethod
    {
        switch self
        {
            case .News:             return .GET
            case .NewsContent(_):   return .GET
        }
    }
}



class SFRequestManager: NSObject, URLSessionDelegate
{
    
    let configuration = URLSessionConfiguration.default
    let timeoutInterval = 60.0
    let cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
    let serializer = SFJsonSerializer()
    
    lazy var session:URLSession = {
            return URLSession(configuration: self.configuration,
                              delegate: self,
                              delegateQueue: OperationQueue.main)
    }()

    func performRequest(method:TCSAPIMethod,
                        success: @escaping (AnyObject) -> Void,
                        failure:@escaping (Error) -> Void)
    {
        let request = NSMutableURLRequest(url: method.methodURL(),
                                          cachePolicy: cachePolicy,
                                          timeoutInterval: timeoutInterval)
        request.httpMethod = method.HTTPMethod().rawValue
        
        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) in
            
            if let unwrappedError = error {
                print(unwrappedError)
                DispatchQueue.main.async {
                    failure(unwrappedError)
                }
                return
            }
            do
            {
                let resultPayload = try self.serializer.deserializeData(data: data)
                DispatchQueue.main.async {
                    success(resultPayload as AnyObject)
                }
            }
            catch
            {
                DispatchQueue.main.async {
                    failure(error)
                }
            }
        }
        dataTask.resume()
    }
}
