//
//  File.swift
//  Tinkoff-news
//
//  Created by SMFedorov on 06/03/2017.
//  Copyright © 2017 Sergey Fedorov. All rights reserved.
//

import Foundation

enum JSONDeserializationError: Error
{
    case PayloadMissing
    case ResultCodeError
    case EmptyResponse
}


class SFJsonSerializer
{
    func deserializeData(data: Data?) throws -> AnyObject
    {
        var deserializationResult : Any?
        
        do
        {
            deserializationResult = try JSONSerialization.jsonObject(with: data!,
                                                                     options: [])
            
            if let deserializedDictionary = deserializationResult as? [String : AnyObject] {
                if let resultCode = deserializedDictionary["resultCode"] as? String, resultCode == "OK" {
                    if let resultPayload = deserializedDictionary["payload"] {
                        return resultPayload
                    } else {
                        print("Payload is missing")
                        throw JSONDeserializationError.PayloadMissing
                    }
                } else {
                    print("Result code is wrong or missing")
                    throw JSONDeserializationError.ResultCodeError
                }
            } else {
                print("There is no data in parsed JSON")
                throw JSONDeserializationError.EmptyResponse
            }
            
        }
        catch
        {
            print("Unable to deserialize JSON: \(error)")
            throw error
        }
    }
}

