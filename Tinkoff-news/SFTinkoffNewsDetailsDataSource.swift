//
//  SFTinkoffNewsDetailsDataSource.swift
//  Tinkoff-news
//
//  Created by SMFedorov on 09/03/2017.
//  Copyright © 2017 Sergey Fedorov. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class SFTinkoffNewsDetailsDataSource : NSObject
{
    private var newsID : String
    var delegate: SFErrorHandlingDelegate?
    
    let newsService = SFNewsService()
    
    init(detailedObjectID: String)
    {
        self.newsID = detailedObjectID
    }

    func loadData(completion: @escaping (NewsEntity) -> Void)
    {
        newsService.updateNewsDetails(newsID: self.newsID, completion: {(result) in
            completion(result)
        }, failure: { [weak self] (error) in
            self?.delegate?.didFailToUpdateNews(error: error)
        })
    }
}
