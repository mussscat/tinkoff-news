//
//  SFNewsListDataSource.swift
//  Tinkoff-news
//
//  Created by SMFedorov on 09/03/2017.
//  Copyright © 2017 Sergey Fedorov. All rights reserved.
//

import Foundation
import UIKit
import CoreData

enum NewsEntityKeys : String
{
    case ID = "id"
    case name = "name"
    case publicationDate = "publicationDate"
    case text = "text"
    case contentText = "content"
}

protocol SFNewsListDataSourceDelegate
{
    func didUpdateNews()
    func newsListChanged()
}

class SFNewsListDataSource : NSObject, UITableViewDataSource, NSFetchedResultsControllerDelegate
{
    var delegate : SFNewsListDataSourceDelegate?
    var errorHandlingDelegate : SFErrorHandlingDelegate?
    
    let requestManager = SFRequestManager()
    let newsService = SFNewsService()
    
    lazy var fetchedResultsController : NSFetchedResultsController<NewsEntity> = {
        let fr = NSFetchRequest<NewsEntity>(entityName: TinkoffNewsEntity.News.rawValue)
        fr.sortDescriptors = [NSSortDescriptor(key: NewsEntityKeys.publicationDate.rawValue, ascending: false)]
        let frc = NSFetchedResultsController(fetchRequest: fr,
                                             managedObjectContext: self.newsService.coreDataManager.managedObjectContext,
                                             sectionNameKeyPath: nil,
                                             cacheName: nil)
        do {
            try frc.performFetch()
        } catch {
            print("Error fetching data from database: \(error)")
        }
        return frc
    }()
    
    func updateNews()
    {
        self.fetchedResultsController.delegate = self
        
        self.newsService.updateNewsList(success: { [weak self] in
            self?.delegate?.didUpdateNews()
        }, failure: { [weak self] (error) in
            self?.errorHandlingDelegate?.didFailToUpdateNews(error: error)
            })
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let dateFormatter = DateFormatter()
        let cell = tableView.dequeueReusableCell(withIdentifier: "news-cell", for: indexPath)
        
        let cellText = newsObject(indexPath: indexPath).text
        let cellDetails = dateFormatter.convertDateToString(date: newsObject(indexPath: indexPath).publicationDate as! Date)
        
        cell.textLabel?.text = cellText
        cell.detailTextLabel?.text = cellDetails
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.newsNumber()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func newsObject(indexPath: IndexPath) -> NewsEntity
    {
        return self.fetchedResultsController.object(at: indexPath)
    }
    
    private func newsNumber() -> Int
    {
        if let number = self.fetchedResultsController.sections?.first?.numberOfObjects
        {
            return number
        }
        else
        {
            return 0
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>)
    {
        self.delegate?.newsListChanged()
    }
    
}
