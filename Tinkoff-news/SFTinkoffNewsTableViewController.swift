//
//  SFTinkoffNewsTableViewController.swift
//  Tinkoff-news
//
//  Created by SMFedorov on 06/03/2017.
//  Copyright © 2017 Sergey Fedorov. All rights reserved.
//

import UIKit

class SFTinkoffNewsTableViewController: UITableViewController, SFNewsListDataSourceDelegate, SFErrorHandlingDelegate
{
    var dataSource : SFNewsListDataSource = SFNewsListDataSource()

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.dataSource.delegate = self
        self.dataSource.errorHandlingDelegate = self
        self.tableView.dataSource = self.dataSource
        self.tableView.delegate = self
        
        self.tableView.rowHeight = UITableViewAutomaticDimension;
        self.tableView.estimatedRowHeight = 40.0;
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.addTarget(self, action: #selector(refreshControlAction(sender:)), for: .valueChanged)
        self.navigationItem.title = "Tinkoff News"
        
        self.dataSource.updateNews()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        guard let destinationVC = segue.destination as? SFTinkoffNewsDetails else {
            assert(false, "Wrong segue")
        }
        
        destinationVC.detailedObject = self.dataSource.newsObject(indexPath: self.tableView.indexPathForSelectedRow!)
        
    }
    
    func didUpdateNews()
    {
        self.refreshControl?.endRefreshing()
    }
    
    func didFailToUpdateNews(error: Error)
    {
        self.refreshControl?.endRefreshing()
        self.present(addAlertErrorHandler(error: error),
                     animated: false,
                     completion: nil)
    }
    
    func newsListChanged()
    {
        self.tableView.reloadData()
    }
    
    func refreshControlAction(sender : Any?)
    {
        self.dataSource.updateNews()
    }
    
}

