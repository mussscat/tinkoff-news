//
//  SFTinkoffNewsDetails.swift
//  Tinkoff-news
//
//  Created by SMFedorov on 06/03/2017.
//  Copyright © 2017 Sergey Fedorov. All rights reserved.
//

import Foundation
import UIKit

class SFTinkoffNewsDetails : UIViewController, SFErrorHandlingDelegate
{
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var publicationDateLabel: UILabel!
    @IBOutlet weak var contentText: UILabel!
    
    var detailedObject : NewsEntity!
    var dataSource : SFTinkoffNewsDetailsDataSource!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.dataSource = SFTinkoffNewsDetailsDataSource(detailedObjectID: self.detailedObject.id!)
        self.dataSource.delegate = self
        self.dataSource.loadData { [weak self] (object) in
            self?.updateLabels(newsObject: object)
        }
    }
    
    func updateLabels(newsObject: NewsEntity)
    {
        let dateFormater = DateFormatter()
        let objectToDisplay = newsObject
        
        self.textLabel.text = objectToDisplay.text
        self.publicationDateLabel.text = dateFormater.convertDateToString(date: objectToDisplay.publicationDate as! Date)
        self.contentText.text = objectToDisplay.contentText
    }

    func didFailToUpdateNews(error: Error)
    {
        self.present(addAlertErrorHandler(error: error),
                     animated: false,
                     completion: nil)
    }
}
